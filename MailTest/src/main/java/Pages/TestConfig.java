package Pages;

import org.aeonbits.owner.Config;
@Config.Sources("classpath:config.properties")

public interface TestConfig extends Config {
    String url();
    String login();
    String pass();
    String subj();
    String mailText();
    String receiver();
}
