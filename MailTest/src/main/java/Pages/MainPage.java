package Pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.page.Page;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
public class MainPage extends Page {

    public MainPage() {
        PageFactory.initElements(Singleton.getDriver(), this);
         }

        @FindBy (css = "[class*='topMN']")
        private WebElement topPanel;
        @FindBy(css= "#id_send_email")
        private WebElement writeMailBtn;
        @FindBy(css="#id_delete")
        private WebElement deleteMailBtn;
        @FindBy(css = "[class*='treeElement grayFon']")
        private WebElement incomeMail;
        @FindBy(css="#left > div:nth-child(3) > div.left_boxes > div:nth-child(3) > div > div.treeElement")
        private WebElement outcomeMail;
        @FindBy (css="#left > div:nth-child(3) > div.left_boxes > div:nth-child(5) > div > div.treeElement")
        private WebElement deletedMail;
        @FindBy(css="#allcheck")
        private  WebElement checkAll;
        @FindBy(css="#id_logout")
        private WebElement logout;

        public void waitForElem(){
            new WebDriverWait(Singleton.getDriver(),10).until(ExpectedConditions.visibilityOf(topPanel));
        }
        public void writeNewEmail(){

            writeMailBtn.click();
        }
        public String getCountIncomeMail(){
           String count= incomeMail.getText();
           return count;
        }
        public String getCountOutcomeMail(){
        String count= outcomeMail.getText();
        return count;
        }
        public String getCountDeletedMail(){
            String count=deletedMail.getText();
            return count;
        }
        public void deleteAllMails(){
            checkAll.click();
            deleteMailBtn.click();
    }





}
