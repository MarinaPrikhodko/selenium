package Pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Singleton {


        private static WebDriver driver;
        public static WebDriver getDriver() {
            if (driver == null) {
                driver = new ChromeDriver();
            }
            return driver;
        }
    }

