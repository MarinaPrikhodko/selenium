package Pages;


import lombok.*;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.page.Page;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class LoginPage extends Page  {
    TestConfig cnf= ConfigFactory.create(TestConfig.class);



        public LoginPage() {
            PageFactory.initElements(Singleton.getDriver(), this);
             }

        @FindBy(css="[class*='mail-sign-in btn btn-primary']")
        private  WebElement mailSignIn;

        @FindBy(css = "[class*='btn btn-primary btn-login']")
                private WebElement loginBtn;

        @FindBy(css = "[class*='form-control'][name='login']")
        private WebElement loginField;

        @FindBy (css="[class*='form-control'][name='password']")
        private WebElement passField;

        public void open(){
            Singleton.getDriver().get(cnf.url());
        }

        public void clickMailSignIn(){
            mailSignIn.click();
        }
         public void enterLogin(){
            loginField.click();
            loginField.sendKeys(cnf.login());
         }
         public void enterPass(){
            passField.click();
            passField.sendKeys(cnf.pass());
        }
        public void logIn(){
            loginBtn.click();
        }




    }


