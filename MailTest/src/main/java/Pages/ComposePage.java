package Pages;

import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.page.Page;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComposePage extends Page {

    TestConfig cnf= ConfigFactory.create(TestConfig.class);

    public ComposePage() {
        PageFactory.initElements(Singleton.getDriver(), this);
       }
       @FindBy (css="[class*='panel_submit'][name='send']")
        private WebElement sendBtn;
    @FindBy (css="#send_to")
    private WebElement addressField;
    @FindBy(css="#subject")
    private WebElement subjectField;
    @FindBy(css="#body")
    private WebElement bodyMailField;

    public void waitForElem(){
        new WebDriverWait(Singleton.getDriver(),10).until(ExpectedConditions.visibilityOf(sendBtn));
    }

    public void writingMail(){
        addressField.click();
        addressField.sendKeys(cnf.receiver());
        subjectField.click();
        subjectField.sendKeys(cnf.subj());
        bodyMailField.click();
        bodyMailField.sendKeys(cnf.mailText());
        sendBtn.click();

    }

}
