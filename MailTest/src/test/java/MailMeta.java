import Pages.ComposePage;
import Pages.LoginPage;
import Pages.MainPage;
import Pages.Singleton;
import io.github.bonigarcia.wdm.WebDriverManager;

import junit.framework.Assert;
import org.junit.*;

public class MailMeta {


    @BeforeClass
    public static void start()  {
        WebDriverManager.chromedriver().setup();
    }
    @AfterClass
    public static void quit(){
      Singleton.getDriver().quit();

    }


    @Test
    public  void test1(){
        /* сравнить количество входящих писем до отправки и после */

        MainPage mainPage=new MainPage();
        ComposePage composePage=new ComposePage();
        LoginPage login=new LoginPage();
        login.open();
        login.clickMailSignIn();
        login.enterLogin();
        login.enterPass();
        login.logIn();
        mainPage.waitForElem();
        String beforeSendMail=mainPage. getCountIncomeMail().split("/")[1].trim();
        System.out.println(beforeSendMail);
        mainPage.writeNewEmail();
        composePage.waitForElem();
        composePage.writingMail();
        mainPage.waitForElem();
        String afterSendMail=mainPage.getCountIncomeMail().split("/")[1].trim();
        System.out.println(afterSendMail);

        Assert.assertEquals(Integer.parseInt(beforeSendMail)+1,Integer.parseInt(afterSendMail));
         mainPage.getLogout().click();
    }

    @Test
    public void test2(){
        /* сравнить количество писем в корзине до удаления и после*/


        LoginPage login=new LoginPage();
        MainPage mainPage=new MainPage();
        login.open();
        login.clickMailSignIn();
        login.enterLogin();
        login.enterPass();
        login.logIn();
        mainPage.waitForElem();
        String countIncomeMail=mainPage.getCountIncomeMail().split("/")[1].trim();
        System.out.println(countIncomeMail);
        String beforeDeleteMail=mainPage.getCountDeletedMail().split("/")[1].trim();
        System.out.println(beforeDeleteMail);
        mainPage.deleteAllMails();
        mainPage.waitForElem();
        String afterDeleteMail=mainPage.getCountDeletedMail().split("/")[1].trim();
        System.out.println(afterDeleteMail);

        Assert.assertEquals(Integer.parseInt(beforeDeleteMail)+Integer.parseInt(countIncomeMail),Integer.parseInt(afterDeleteMail));
        mainPage.getLogout().click();
    }

    @Test
    public void test3(){
        /*сравнить количетво отправленых писем до отправки и после*/
        Singleton.getDriver().get("https://meta.ua/");
        LoginPage login=new LoginPage();
        MainPage mainPage=new MainPage();
        ComposePage composePage=new ComposePage();
        login.open();
        login.clickMailSignIn();
        login.enterLogin();
        login.enterPass();
        login.logIn();
        mainPage.waitForElem();
        String countOutcomeMailBeforeSending=mainPage.getCountOutcomeMail();
        System.out.println(countOutcomeMailBeforeSending);
        mainPage.writeNewEmail();
        composePage.waitForElem();
        composePage.writingMail();
        mainPage.waitForElem();
        String countOutcomeMailAfterSending=mainPage.getCountOutcomeMail();
        System.out.println(countOutcomeMailBeforeSending);

        Assert.assertEquals(Integer.parseInt(countOutcomeMailBeforeSending)+1,Integer.parseInt(countOutcomeMailAfterSending));
        mainPage.getLogout().click();
}


}
